<div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
					<li>
                        <a href="<?php echo base_url('produk'); ?>" class=" hvr-bounce-to-right"><i class="fa fa-inbox nav_icon"></i> <span class="nav-label">Produk</span> </a>
                    </li>
                    
                    <li>
                        <a href="<?php echo base_url('brand'); ?>" class=" hvr-bounce-to-right"><i class="fa fa-picture-o nav_icon"></i> <span class="nav-label">Brand</span> </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('kategori'); ?>" class=" hvr-bounce-to-right"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">Kategori</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('pesanan'); ?>" class=" hvr-bounce-to-right"><i class="fa fa-cart-plus nav_icon"></i> <span class="nav-label">Pesanan</span> </a>
                    </li>
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Rekomendasi</span></a>
                    </li>
                </ul>
            </div>
			</div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">