<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Dashboard extends CI_Controller {

	public function __construct()
    {
       parent::__construct();
        //load model admin
        $this->load->model('M_Admin');
        //cek session dan level user
        if($this->M_Admin->is_role() != "admin"){
            redirect("login");
        }
    }

    // mengambil data dari model
	public function index()
	{
		$data["kecamatan"] = $this->M_Admin->getAll();
        $data["jumlah_kec"] = $this->M_Admin->getJumlahKec();
        $data["luas"] = $this->M_Admin->getLuas();
        $data["penduduk"] = $this->M_Admin->getPenduduk();
        $this->load->view("admin/V_Dashboard", $data); 
	}

    // fungsi logout
	public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
