<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Dashboard extends CI_Controller {

	public function __construct()
    {
       parent::__construct();
        //load model admin
        $this->load->model('M_Admin');
        //cek session dan level user
        if($this->M_Admin->is_role() != "user"){
            redirect("login");
        }
        // load model user
        $this->load->model('M_User');
    }

	public function index()
	{
		$data["kelurahan"] = $this->M_User->getAll();
        $data["jumlah_kel"] = $this->M_User->getJumlahKel();
        $data["luas"] = $this->M_User->getLuas();
        $data["penduduk"] = $this->M_User->getPenduduk();
        $this->load->view("user/V_Dashboard", $data); 
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
