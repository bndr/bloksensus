<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin extends CI_Model
{
    // menetapkan tabel
    private $_table = "kecamatan";

    // mengecek fungsi logged in
    public function is_logged_in()
    {
        return $this->session->userdata('user_id');
    }

    // mengecek level user
    public function is_role()
    {
        return $this->session->userdata('role');
    }

    // mengecek fungsi login
    public function check_login($table, $field1, $field2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    // mengambil data dari tabel kelurahan
    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    // mengambil data berdasarkan id
    public function getById($id)
    {
        return $this->db->get($this->_table, ["id_kec" => $id])->row();
    }

    // mendapatkan data jumlah kecamatan
    public function getJumlahKec()
    {
        return $this->db->count_all_results($this->_table);
    }

    // mendapatkan data luas kecamatan
    public function getLuas()
    {
        $this->db->select_sum('luas_kec');
        $hasil = $this->db->get($this->_table)->row();
        return $hasil->luas_kec;
    }

    // mendapatkan data jumlah penduduk
    public function getPenduduk()
    {
        $this->db->select_sum('penduduk_kec');
        $hasil = $this->db->get($this->_table)->row();
        return $hasil->penduduk_kec;
    }
}