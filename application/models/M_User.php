<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model
{
    // menetapkan tabel
    private $_table = "kelurahan";

    // mengambil data dari tabel kelurahan
    public function getAll()
    {
        $sesi = $this->session->userdata("user_name");
        $this->db->select('kecamatan.username, kelurahan.id_kel, kelurahan.nama_kel, kelurahan.luas_kel, kelurahan.penduduk_kel');
        $this->db->from('kecamatan, kelurahan');
        $this->db->where('kecamatan.id_kec = kelurahan.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        return $this->db->get()->result();
    }

    // mengambil data berdasarkan id
    public function getById($id)
    {
        return $this->db->get($this->_table, ["id_kel" => $id])->row();
    }

    // mendapatkan data jumlah kelurahan
    public function getJumlahKeL()
    {
        $sesi = $this->session->userdata("user_name");
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = kelurahan.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        return $this->db->count_all_results($this->_table);
    }

    // mendapatkan data luas kelurahan
    public function getLuas()
    {
        $sesi = $this->session->userdata("user_name");
        $this->db->select_sum('luas_kel');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = kelurahan.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        $hasil = $this->db->get($this->_table)->row();
        return $hasil->luas_kel;
    }

    // mendapatkan data jumlah kelurahan
    public function getPenduduk()
    {
        $sesi = $this->session->userdata("user_name");
        $this->db->select_sum('penduduk_kel');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = kelurahan.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        $hasil = $this->db->get($this->_table)->row();
        return $hasil->penduduk_kel;
        // $this->db->select_sum('penduduk_kel');
        // $hasil = $this->db->get($this->_table)->row();
        // return $hasil->penduduk_kel;
    }
}