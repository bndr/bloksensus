-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 03:57 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bloksensus`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_user` varchar(255) NOT NULL,
  `role` enum('admin','user','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `nama_user`, `role`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Administrator', 'admin'),
(2, 'gayungan', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Gayungan', 'user'),
(3, 'asemrowo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Asemrowo', 'user'),
(4, 'benowo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Benowo', 'user'),
(5, 'lakarsantri', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Lakarsantri', 'user'),
(6, 'pakal', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Pakal', 'user'),
(7, 'sambikerep', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Sambikerep', 'user'),
(8, 'sukomanunggal', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Suko Manunggal', 'user'),
(9, 'tandes', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Tandes', 'user'),
(10, 'dukuhpakis', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Dukuh Pakis', 'user'),
(11, 'jambangan', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Jambangan', 'user'),
(12, 'karangpilang', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Karang Pilang', 'user'),
(13, 'sawahan', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Sawahan', 'user'),
(14, 'wiyung', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Wiyung', 'user'),
(15, 'wonocolo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Wonocolo', 'user'),
(16, 'wonokromo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Wonokromo', 'user'),
(17, 'gubeng', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Gubeng', 'user'),
(18, 'gununganyar', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Gunung Anyar', 'user'),
(19, 'mulyorejo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Mulyorejo', 'user'),
(20, 'rungkut', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Rungkut', 'user'),
(21, 'sukolilo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Sukolilo', 'user'),
(22, 'tenggilismejoyo', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Tenggilis Mejoyo', 'user'),
(23, 'bulak', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Bulak', 'user'),
(24, 'kenjeran', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Kenjeran', 'user'),
(25, 'krembangan', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Krembangan', 'user'),
(26, 'pabeancantian', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Pabean Cantian', 'user'),
(27, 'semampir', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Semampir', 'user'),
(28, 'bubutan', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Bubutan', 'user'),
(29, 'genteng', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Genteng', 'user'),
(30, 'tegalsari', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Tegalsari', 'user'),
(31, 'tambaksari', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Tambaksari', 'user'),
(32, 'simokerto', 'e10adc3949ba59abbe56e057f20f883e', 'Kecamatan Simokerto', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kec` varchar(255) NOT NULL,
  `nama_kec` varchar(255) NOT NULL,
  `luas_kec` decimal(6,2) DEFAULT NULL,
  `penduduk_kec` int(7) NOT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kec`, `nama_kec`, `luas_kec`, `penduduk_kec`, `username`) VALUES
('010', 'Kecamatan Karangpilang', '9.24', 75658, 'karangpilang'),
('020', 'Kecamatan Jambangan', '4.19', 52950, 'jambangan'),
('030', 'Kecamatan Gayungan', '6.16', 47064, 'gayungan'),
('040', 'Kecamatan Wonocolo', '6.11', 83749, 'wonocolo'),
('050', 'Kecamatan Tenggilis Mejoyo', '5.47', 59555, 'tenggilismejoyo'),
('060', 'Kecamatan Gunung Anyar', '9.20', 58714, 'gununganyar'),
('070', 'Kecamatan Rungkut', '21.02', 117591, 'rungkut'),
('080', 'Kecamatan Sukolilo', '23.66', 114309, 'sukolilo'),
('090', 'Kecamatan Mulyorejo', '11.94', 90734, 'mulyorejo'),
('100', 'Kecamatan Gubeng', '7.48', 132123, 'gubeng'),
('110', 'Kecamatan Wonokromo', '6.70', 167720, 'wonokromo'),
('120', 'Kecamatan Dukuh Pakis', '10.02', 62520, 'dukuhpakis'),
('130', 'Kecamatan Wiyung', '11.52', 72720, 'wiyung'),
('140', 'Kecamatan Lakarsantri', '17.73', 59930, 'lakarsantri'),
('141', 'Kecamatan Sambikerep', '18.93', 60924, 'sambikerep'),
('150', 'Kecamatan Tandes', '9.77', 98297, 'tandes'),
('160', 'Kecamatan Suko Manunggal', '11.20', 105917, 'sukomanunggal'),
('170', 'Kecamatan Sawahan', '7.64', 213760, 'sawahan'),
('180', 'Kecamatan Tegalsari', '4.29', 117492, 'tegalsari'),
('190', 'Kecamatan Genteng', '3.41', 61934, 'genteng'),
('200', 'Kecamatan Tambaksari', '9.10', 234473, 'tambaksari'),
('210', 'Kecamatan Kenjeran', '7.72', 172174, 'kenjeran'),
('211', 'Kecamatan Bulak', '5.65', 44168, 'bulak'),
('220', 'Kecamatan Simokerto', '2.67', 102764, 'simokerto'),
('230', 'Kecamatan Semampir', '6.14', 202029, 'semampir'),
('240', 'Kecamatan Pabean Cantian', '4.32', 89881, 'pabeancantian'),
('250', 'Kecamatan Bubutan', '3.76', 106399, 'bubutan'),
('260', 'Kecamatan Krembangan', '6.61', 124419, 'krembangan'),
('270', 'Kecamatan Asemrowo', '13.93', 49606, 'asemrowo'),
('280', 'Kecamatan Benowo', '23.76', 66062, 'benowo'),
('281', 'Kecamatan Pakal', '17.59', 56453, 'pakal');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kel` varchar(255) NOT NULL,
  `id_kec` varchar(255) DEFAULT NULL,
  `nama_kel` varchar(255) NOT NULL,
  `luas_kel` decimal(6,2) DEFAULT NULL,
  `penduduk_kel` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `nama_kel`, `luas_kel`, `penduduk_kel`) VALUES
('010-001', '010', 'Kelurahan Warugunung', '3.86', 9198),
('010-002', '010', 'Kelurahan Karang Pilang', '1.44', 9569),
('010-003', '010', 'Kelurahan Kebraon', '2.08', 29482),
('010-004', '010', 'Kelurahan Kedurus', '1.86', 27409),
('020-001', '020', 'Kelurahan Pagesangan', '1.11', 13932),
('020-002', '020', 'Kelurahan Kebonsari', '0.85', 10390),
('020-003', '020', 'Kelurahan Jambangan', '0.73', 10300),
('020-004', '020', 'Kelurahan Karah', '1.50', 18328),
('030-001', '030', 'Kelurahan Dukuh Menanggal', '1.06', 8947),
('030-002', '030', 'Kelurahan Menanggal', '0.66', 9789),
('030-003', '030', 'Kelurahan Gayungan', '1.47', 12220),
('030-004', '030', 'Kelurahan Ketintang', '2.97', 16108),
('040-001', '040', 'Kelurahan Siwalankerto', '1.98', 17683),
('040-002', '040', 'Kelurahan Jemur Wonosari', '1.64', 22771),
('040-003', '040', 'Kelurahan Margorejo', '0.65', 11568),
('040-004', '040', 'Kelurahan Bendul Merisi', '0.77', 17678),
('040-005', '040', 'Kelurahan Sidosermo', '1.07', 14049),
('050-001', '050', 'Kelurahan Kutisari', '1.96', 19290),
('050-002', '050', 'Kelurahan Kendangsari', '1.31', 15464),
('050-003', '050', 'Kelurahan Tenggilis Mejoyo', '0.94', 11340),
('050-004', '050', 'Kelurahan Panjang Jiwo', '1.26', 13461),
('060-001', '060', 'Kelurahan Rungkut Menanggal', '0.92', 13879),
('060-002', '060', 'Kelurahan Rungkut Tengah', '0.93', 13521),
('060-003', '060', 'Kelurahan Gunung Anyar', '2.94', 21376),
('060-004', '060', 'Kelurahan Gunung Anyar Tambak', '4.41', 9938),
('070-001', '070', 'Kelurahan Rungkut Kidul', '1.37', 13802),
('070-002', '070', 'Kelurahan Medokan Ayu', '7.23', 26802),
('070-003', '070', 'Kelurahan Wonorejo', '6.48', 17424),
('070-004', '070', 'Kelurahan Penjaringan Sari', '1.81', 19367),
('070-005', '070', 'Kelurahan Kedung Baruk', '1.55', 16537),
('070-006', '070', 'Kelurahan Kali Rungkut', '2.58', 23659),
('080-001', '080', 'Kelurahan Nginden Jangkungan', '1.14', 15907),
('080-002', '080', 'Kelurahan Semolowaru', '1.67', 19683),
('080-003', '080', 'Kelurahan Medokan Semampir', '1.87', 19331),
('080-004', '080', 'Kelurahan Keputih', '14.40', 17425),
('080-005', '080', 'Kelurahan Gebang Putih', '1.33', 7614),
('080-006', '080', 'Kelurahan Klampis Ngasem', '1.68', 17805),
('080-007', '080', 'Kelurahan Menur Pumpungan', '1.57', 16544),
('090-001', '090', 'Kelurahan Manyar Sabrangan', '1.13', 17867),
('090-002', '090', 'Kelurahan Mulyorejo', '3.01', 18622),
('090-003', '090', 'Kelurahan Kejawan Putih Tambak', '2.21', 7406),
('090-004', '090', 'Kelurahan Kalisari', '2.13', 15887),
('090-005', '090', 'Kelurahan Dukuh Sutorejo', '2.14', 16791),
('090-006', '090', 'Kelurahan Kalijudan', '1.32', 14161),
('100-001', '100', 'Kelurahan Barata Jaya', '0.76', 16772),
('100-002', '100', 'Kelurahan Pucang Sewu', '0.94', 14926),
('100-003', '100', 'Kelurahan Kertajaya', '1.30', 16772),
('100-004', '100', 'Kelurahan Gubeng', '1.10', 15134),
('100-005', '100', 'Kelurahan Airlangga', '1.62', 20915),
('100-006', '100', 'Kelurahan Mojo', '1.76', 47604),
('110-001', '110', 'Kelurahan Sawunggaling', '1.50', 28714),
('110-002', '110', 'Kelurahan Wonokromo', '1.00', 42853),
('110-003', '110', 'Kelurahan Jagir', '1.03', 22277),
('110-004', '110', 'Kelurahan Ngagel Rejo', '1.36', 44909),
('110-005', '110', 'Kelurahan Ngagel', '0.86', 12839),
('110-006', '110', 'Kelurahan Darmo', '0.95', 16128),
('120-001', '120', 'Kelurahan Gunung Sari', '1.63', 15014),
('120-002', '120', 'Kelurahan Dukuh Pakis', '3.07', 15431),
('120-003', '120', 'Kelurahan Pradah Kali Kendal', '3.96', 15882),
('120-004', '120', 'Kelurahan Dukuh Kupang', '1.36', 16193),
('130-001', '130', 'Kelurahan Balas Klumprik', '2.01', 13617),
('130-002', '130', 'Kelurahan Babatan', '4.40', 28958),
('130-003', '130', 'Kelurahan Wiyung', '3.55', 18679),
('130-004', '130', 'Kelurahan Jajar Tunggal', '1.56', 11466),
('140-001', '140', 'Kelurahan Bangkingan', '2.76', 8884),
('140-002', '140', 'Kelurahan Sumur Welut', '2.56', 5305),
('140-003', '140', 'Kelurahan Lidah Wetan', '2.78', 11624),
('140-004', '140', 'Kelurahan Lidah Kulon', '3.85', 17148),
('140-005', '140', 'Kelurahan Jeruk', '2.70', 8605),
('140-006', '140', 'Kelurahan Lakarsantri', '3.08', 8364),
('141-001', '141', 'Kelurahan Made', '4.47', 8519),
('141-002', '141', 'Kelurahan Bringin', '4.11', 5369),
('141-003', '141', 'Kelurahan Sambikerep', '4.50', 18261),
('141-004', '141', 'Kelurahan Lontar', '5.85', 28775),
('150-001', '150', 'Kelurahan Tandes', '1.07', 9900),
('150-002', '150', 'Kelurahan Karangpoh', '1.55', 16439),
('150-003', '150', 'Kelurahan Balongsari', '1.25', 12657),
('150-004', '150', 'Kelurahan Manukan Wetan', '2.88', 9150),
('150-005', '150', 'Kelurahan Manukan Kulon', '2.00', 37384),
('150-006', '150', 'Kelurahan Banjarsugihan', '1.02', 12767),
('160-001', '160', 'Kelurahan Putat Gede', '1.16', 7148),
('160-002', '160', 'Kelurahan Sonokawijenan', '1.13', 8480),
('160-003', '160', 'Kelurahan Simo Mulyo', '2.60', 24171),
('160-004', '160', 'Kelurahan Sukomanunggal', '2.30', 11417),
('160-005', '160', 'Kelurahan Tanjung Sari', '2.01', 12453),
('160-006', '160', 'Kelurahan Simo Mulyo Baru', '2.00', 42248),
('170-001', '170', 'Kelurahan Pakis', '2.47', 38630),
('170-002', '170', 'Kelurahan Putat Jaya', '1.36', 48311),
('170-003', '170', 'Kelurahan Banyu Urip', '0.96', 42061),
('170-004', '170', 'Kelurahan Kupang Krajan', '0.60', 25376),
('170-005', '170', 'Kelurahan Petemon', '1.35', 38933),
('170-006', '170', 'Kelurahan Sawahan', '0.90', 20449),
('180-001', '180', 'Kelurahan Keputran', '0.96', 20658),
('180-002', '180', 'Kelurahan Dr. Sutomo', '1.38', 22861),
('180-003', '180', 'Kelurahan Tegalsari', '0.53', 22185),
('180-004', '180', 'Kelurahan Wonorejo', '0.68', 26521),
('180-005', '180', 'Kelurahan Kedungdoro', '0.74', 25267),
('190-001', '190', 'Kelurahan Embong Kaliasin', '1.10', 13191),
('190-002', '190', 'Kelurahan Ketabang', '0.98', 7546),
('190-003', '190', 'Kelurahan Genteng', '0.53', 8562),
('190-004', '190', 'Kelurahan Peneleh', '0.45', 15101),
('190-005', '190', 'Kelurahan Kapasari', '0.35', 17534),
('200-001', '200', 'Kelurahan Pacar Keling', '0.70', 23763),
('200-002', '200', 'Kelurahan Pasar Kembang\r\n', '2.09', 41248),
('200-003', '200', 'Kelurahan Ploso', '1.49', 35918),
('200-004', '200', 'Kelurahan Tambak Sari', '0.63', 20696),
('200-005', '200', 'Kelurahan Rangkah', '0.70', 18408),
('200-006', '200', 'Kelurahan Gading', '0.79', 30833),
('200-007', '200', 'Kelurahan Kapas Madya Baru', '1.58', 41596),
('200-008', '200', 'Kelurahan Dukuh Setro', '1.12', 22011),
('210-005', '210', 'Kelurahan Tanah Kalikedinding', '2.41', 58346),
('210-006', '210', 'Kelurahan Sidotopo Wetan', '1.66', 62105),
('210-007', '210', 'Kelurahan Bulak Banteng', '2.67', 34799),
('210-008', '210', 'Kelurahan Tambak Wedi', '0.98', 16924),
('211-001', '211', 'Kelurahan Sukolilo Baru', '2.70', 20097),
('211-002', '211', 'Kelurahan Kenjeran', '0.71', 6076),
('211-003', '211', 'Kelurahan Bulak', '1.32', 6773),
('211-004', '211', 'Kelurahan Kedung Cowek', '0.92', 11222),
('220-001', '220', 'Kelurahan Kapasan', '0.51', 16727),
('220-002', '220', 'Kelurahan Tambak Rejo', '0.61', 20781),
('220-003', '220', 'Kelurahan Simokerto', '0.86', 22952),
('220-004', '220', 'Kelurahan Sidodadi', '0.28', 18247),
('220-005', '220', 'Kelurahan Simolawang', '0.41', 24057),
('230-001', '230', 'Kelurahan Ampel', '0.38', 21124),
('230-002', '230', 'Kelurahan Sidotopo', '2.98', 35372),
('230-003', '230', 'Kelurahan Pegirian', '0.40', 34425),
('230-004', '230', 'Kelurahan Wonokusumo', '0.76', 74665),
('230-005', '230', 'Kelurahan Ujung', '1.62', 36443),
('240-001', '240', 'Kelurahan Bongkaran', '0.90', 12908),
('240-002', '240', 'Kelurahan Nyamplungan', '0.55', 11758),
('240-003', '240', 'Kelurahan Krembangan Utara', '0.68', 18699),
('240-004', '240', 'Kelurahan Perak Timur', '0.40', 16141),
('240-005', '240', 'Kelurahan Perak Utara', '1.79', 30375),
('250-001', '250', 'Kelurahan Tembok Dukuh', '0.83', 27373),
('250-002', '250', 'Kelurahan Bubutan', '0.60', 14724),
('250-003', '250', 'Keluran Alon-alon Contong', '0.65', 7077),
('250-004', '250', 'Kelurahan Gundih', '0.85', 29823),
('250-005', '250', 'Kelurahan Jepara', '0.83', 27402),
('260-001', '260', 'Kelurahan Dupak', '0.48', 25071),
('260-002', '260', 'Kelurahan Morokrembangan', '3.17', 47940),
('260-003', '260', 'Kelurahan Perak Barat', '1.61', 16655),
('260-004', '260', 'Kelurahan Kemayoran', '0.51', 19383),
('260-005', '260', 'Kelurahan Krembangan Selatan', '0.84', 15370),
('270-001', '270', 'Kelurahan Tambak Sarioso', '6.47', 7599),
('270-002', '270', 'Kelurahan Asemrowo', '3.39', 33642),
('270-003', '270', 'Kelurahan Genting Kalianak', '4.07', 8365),
('280-001', '280', 'Kelurahan Sememi', '4.11', 37036),
('280-002', '280', 'Kelurahan Kandangan', '3.61', 22530),
('280-003', '280', 'Kelurahan Tambak Oso Wilangun', '8.46', 3852),
('280-004', '280', 'Kelurahan Romo Kalisari', '7.58', 2644),
('281-001', '281', 'Kelurahan Babat Jerawat', '4.40', 24159),
('281-002', '281', 'Kelurahan Pakal', '3.71', 9622),
('281-003', '281', 'Kelurahan Benowo', '1.98', 11123),
('281-004', '281', 'Kelurahan Sumberejo', '7.50', 11549);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `uname_kec` (`username`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kec`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kel`),
  ADD KEY `id_kec` (`id_kec`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `uname_kec` FOREIGN KEY (`username`) REFERENCES `akun` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `id_kec` FOREIGN KEY (`id_kec`) REFERENCES `kecamatan` (`id_kec`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
